import {
  Body,
  Controller,
  Delete,
  Get,
  Header,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { ProductsService } from './products.service';
import { Product } from './schemas/product.schema';

/*
В express мы пишем
 server.use((req, res, next) => { res.status(201).end() })

В Nest.js так делать не стоит, но это возможно. Для этого нужно импортировать Req и Req

getAll(@Req() req: Request, @Req() res: Response): string {
res.status(201).end()
}
 */

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {} // инжектим сервис в наш контроллер

  // @Redirect('https://google.com', 301) // можно делать пренаправления на другие url
  @Get()
  getAll(): Promise<Product[]> {
    return this.productsService.getAll();
  }

  @Get(':id') // ожидаем динамический параметр
  getOne(@Param('id') id: string): Promise<Product> {
    // ('id') - указываем какое конкретно свойство нужно получить из объекта
    return this.productsService.getById(id);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @Header('Cache-Control', 'none')
  create(@Body() createProductDto: CreateProductDto): Promise<Product> {
    return this.productsService.create(createProductDto);
  }

  @Delete(':id')
  delete(@Param('id') id: string): Promise<Product> {
    return this.productsService.remove(id);
  }

  @Put(':id')
  update(
    @Body() updateProductDto: UpdateProductDto,
    @Param('id') id: string,
  ): Promise<Product> {
    return this.productsService.update(id, updateProductDto);
  }
}
