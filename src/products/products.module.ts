import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Product, ProductSchema } from './schemas/product.schema';

// Модули - позволяют создавать изолированные сущности
@Module({
  // обязательно нужно объявлять метаданные
  providers: [ProductsService],
  controllers: [ProductsController],
  imports: [
    MongooseModule.forFeature([
      // регистрируем нашу схему
      { name: Product.name, schema: ProductSchema }, // по факту мы создаем коллекцию с названием Product.name и утверждаем нашу схему
    ]),
  ],
})
export class ProductsModule {}
