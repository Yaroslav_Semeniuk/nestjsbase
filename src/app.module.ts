import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductsModule } from './products/products.module';
import { MongooseModule } from '@nestjs/mongoose';
const URI =
  'mongodb+srv://AdminUser:tDHWzcYP01QtLuNf@cluster0.yyfy9.mongodb.net/products?retryWrites=true&w=majority';
// Чтобы подключить Mongo нам необходимо сделать это ИМЕННО в app.module.ts
@Module({
  imports: [ProductsModule, MongooseModule.forRoot(URI)], // импортируем созданые нами модули
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
