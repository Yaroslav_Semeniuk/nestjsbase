import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose'; // ОБЯЗАТЕЛЬНО УКАЗАТЬ ДАННЫЙ ИМПОРТ

export type ProductDocument = Product & Document;

@Schema()
export class Product {
  @Prop() // в декоратор мы можем передавать параметры для доп. настройки полей
  title: string;

  @Prop()
  price: number;
}

export const ProductSchema = SchemaFactory.createForClass(Product);
